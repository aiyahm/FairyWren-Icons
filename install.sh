dir=$(pwd)
git pull
if [ -d "$HOME/.local/share/icons/FairyWren_Dark" ] || [ -d "$HOME/.local/share/icons/FairyWren_Light" ]; then
	echo 'FairyWren Icons are already installed.'
	exit 1
fi
echo 'How would you like to install? options:'
echo '1: Symbolic links - keeps git repo on system and allows use of git pull to keep up to date'
echo '2: Direct copy icons and remove git repo'
read -p 'option: ' choice
if [ "$choice" == "1" ]; then
	ln -s "$dir/FairyWren_Dark" "$HOME/.local/share/icons/FairyWren_Dark"
	ln -s "$dir/FairyWren_Light" "$HOME/.local/share/icons/FairyWren_Light"
elif [ "$choice" == "2" ]; then
	cp -r "$dir/FairyWren_Dark" "$HOME/.local/share/icons/FairyWren_Dark"
	cp -r "$dir/FairyWren_Light" "$HOME/.local/share/icons/FairyWren_Light"
	echo "You are about to remove the directory $dir. Are you sure? (y/n)"
	read confirm
	if [ "$confirm" == "y" ]; then
		rm -rf "$dir"
	else
		echo "Directory removal cancelled."
	fi
else
	echo 'Exiting as no valid option was selected.'
	exit 1
fi
echo 'Thanks for installing Fairywren Icons!'
