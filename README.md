<p align="center">
  <img alt="preview" src="https://gitlab.com/aiyahm/FairyWren-Icons/-/raw/main/Releases/release%20materials/fairywren.icons.banner.png?ref_type=heads"/>
</p>

## FairyWren Icon Set Overview

Fairywren is a free and open source SVG icon theme for Linux, based on [Papirus Icon Set](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme)
The goal of this project is to provide a modernized take on icons a little inspired by the move to more gradient styling in other packs while keeping the material feel.

## Branding Inspiration

The icon packs name and branding is inspired by one of australias native birds: the Splendid Fairywren. I loved seeing these strikingly blue birds on my morning walks down the Torquay beach front. I was never able to get a good photo of them as they moved too fast for me to capture one. as a photographer and avid bird admirer i thought this beautiful bird deserved to be represented in my work. read more about these wonderful birds at [Bird Life Australia](https://birdlife.org.au/bird-profiles/splendid-fairy-wren/)

## Installation

### FairyWren comes with three options for installation.
  - Primary option: 
    - Clone the git repository and run ./install.sh from within the cloned directory. 
  - Snapshot release option: 
    - navigate to: [pling](https://www.pling.com/p/1684521/) and simply click install this uses OCS-URL or the pling store.
  - Desktop Enviroment Direct Install: 
    - Desktop Enviroments with built in icon store options, like plasma, go to your system themeing settings and download it from the store there. 

## Uninstall

  - to uninstall you can use the uninstall script to remove icons from installed directory or simply navigate to ~/.local/share/icons and remove the installed directories

<p align="center">
  <img alt="cinnamon screenshot - Dark" src="https://gitlab.com/aiyahm/FairyWren-Icons/-/raw/main/Releases/release%20materials/release.screenshot.dark.png?ref_type=heads"/>
</p>
<p align="center">
  <img alt="cinnamon screenshot - Light" src="https://gitlab.com/aiyahm/FairyWren-Icons/-/raw/main/Releases/release%20materials/release.screenshot.light.png?ref_type=heads"/>
</p>

## License

FairyWren icon set is distributed under the terms of the GNU General Public License, version 3. See the [`LICENSE`](LICENSE) file for details.

