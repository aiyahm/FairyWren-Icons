echo "You are about to remove FairyWren icons. Are you sure? (y/n)"
read confirm
if [ "$confirm" == "y" ]; then
	rm -rf "$HOME/.local/share/icons/FairyWren_Dark"
	rm -rf "$HOME/.local/share/icons/FairyWren_Light"
else
	echo "uninstall cancelled."
fi
echo "Thanks for using Fairywren Icons!"
echo "if you were having issues please lodge an issue here: https://gitlab.com/aiyahm/FairyWren-Icons/-/issues"